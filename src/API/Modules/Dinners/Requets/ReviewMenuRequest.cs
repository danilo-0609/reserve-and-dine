﻿namespace API.Modules.Dinners.Requets;

public sealed record ReviewMenuRequest(decimal Rate,
    string Comment);
