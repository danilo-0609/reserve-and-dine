﻿namespace API.Modules.Dinners.Requets;

public sealed record UpdateRestaurantRateRequest(int Stars,
    string Comment);
