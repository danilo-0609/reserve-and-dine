﻿using Microsoft.AspNetCore.Authorization;

namespace API.Modules.Users.Policies.Dinners.Menus.Publish;

public sealed class CanPublishAMenuRequirement : IAuthorizationRequirement
{
}
